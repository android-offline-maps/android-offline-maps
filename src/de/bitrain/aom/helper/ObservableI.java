package de.bitrain.aom.helper;

import java.util.Observable;
import java.util.Observer;

/**
 * Interface for observable classes.
 * 
 * It is for classes that would like to extend {@link Observable} but, due to
 * their dependency on another superclass, can't. Instead, they pass all calls
 * to the methods defined below to an internal instance of {@link Observable}.
 */
public interface ObservableI
{
  public void addObserver(Observer observer);

  public int countObservers();

  public void deleteObserver(Observer observer);

  public void deleteObservers();

  public boolean hasChanged();

  public void notifyObservers();

  public void notifyObservers(Object data);
}
