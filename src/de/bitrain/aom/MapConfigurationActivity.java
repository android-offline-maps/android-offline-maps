package de.bitrain.aom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MapConfigurationActivity extends Activity
{
  private final static Logger logger = LoggerFactory
    .getLogger(MapConfigurationActivity.class);

  @Override
  protected void onCreate(final Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    this.setContentView(R.layout.activity_map_configuration);
  }

  public void abortConfiguration_OnClick(final View view)
  {
    finish();
  }

  public void saveConfiguration_OnClick(final View view)
  {
    logger.debug("Save Configuration..");
    final Intent returnIntent = new Intent();
    final EditText edit =
      (EditText) findViewById(R.id.map_configuration_scale_edit);
    float scale;
    int state;
    try
    {
      scale = Float.valueOf(edit.getText().toString());
      state = RESULT_OK;
    }
    catch (NumberFormatException e)
    {
      scale = -1;
      state = RESULT_CANCELED;
    }
    returnIntent.putExtra("ConfigurationChanged", scale);
    setResult(state, returnIntent);
    logger.debug("--> Configuration saved. " + edit.getText().toString());
    finish();
  }
}
