package de.bitrain.aom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.widget.ImageView;
import de.bitrain.aom.position.PositionService;
import de.bitrain.aom.position.PositionService.PositionServiceBinder;


/**
 * Singelton-Pattern. Get the instance with Controller.getInstance().
 * @author Studium
 *
 */
public class Controller
{

  private static Controller mInstance = null;
  private final static Logger mLogger = LoggerFactory
    .getLogger(MainActivity.class);
  private ImageView mImageMap;
  private MainActivity mMainActivity;

  /**
   * For Position Service
   */
  private PositionService mPositionService;
  private boolean mBound = false;

  /**
   * Has user selected a map, this parameter will be true.
   */
  private boolean mMapActive = false;

  /**
   * Scaling factor in meters.
   * If -1 no scaling was selected.
   */
  private float mScaleFactor = -1;

  /**
   * Absolute position on map.
   */
  private static Point mPositionOnMap = null;

  /**
   * Indicates if user has to tap on map to set current location.
   */
  private static boolean mSetLocationMode = false;

  /**
   * Path of the image-map-file.
   */
  private static String mImageFilePath = "";
  private Bitmap mMapBitmap;

  private float mZoomFactor = 1.0f;

  /**
   * Hide constructor because it has to be singelton
   */
  private Controller()
  {

  }

  public static Controller getInstance()
  {
    if (mInstance == null)
      mInstance = new Controller();

    return mInstance;
  }

  /**
   * Binds PositionService to MainActivity and register
   * MainActivity as Observer to PositionService.
   * @param main
   */
  public void bindService(final MainActivity main)
  {
    mLogger.debug("Binding to position service.");
    final Intent trackingIntent = new Intent(main, PositionService.class);
    main.bindService(trackingIntent, mConnection, Context.BIND_AUTO_CREATE);
    mMainActivity = main;
  }

  /**
   * Defines callbacks for service binding, passed to bindService()
   */
  private final ServiceConnection mConnection = new ServiceConnection()
  {

    @Override
    public void onServiceConnected(final ComponentName className,
      final IBinder binder)
    {
      mLogger.debug("Bound to position service.");
      mBound = true;
      mPositionService = ((PositionServiceBinder) binder).getService();
      mPositionService.addObserver(mMainActivity);
    }

    @Override
    public void onServiceDisconnected(final ComponentName name)
    {
      mLogger.debug("Disconnected from service.");
      mBound = false;
    }
  };

  final float ZOOMING = .2f;
  
  /**
   * After zooming, map will be redrawn.
   */
  public void incZoomFactor()
  {
    if ((mZoomFactor + ZOOMING) <= 50)
    {
      mZoomFactor += ZOOMING;
      mImageMap.invalidate();
      mLogger.debug("ZoomFactor neu: " + mZoomFactor);
    }
  }

  /**
   * After zooming, map will be redrawn.
   */
  public void decZoomFactor()
  {
    if ((mZoomFactor - ZOOMING) >= ZOOMING)
    {
      mZoomFactor -= ZOOMING;
      mImageMap.invalidate();
      mLogger.debug("ZoomFactor neu: " + mZoomFactor);
    } 
  }

  public float getZoomFactor()
  {
    return mZoomFactor;
  }

  public float getScaleFactor()
  {
    return mScaleFactor;
  }

  /**
   * 
   * @param value One meter in real is "value" meters on map.
   * 
   */
  public void setScaleFactor(float value)
  {
    mScaleFactor = value;
    mLogger.debug("--> New ScaleFactor: 1m on map --> " + mScaleFactor + "m in real.");
    
    // PositionService needs pixel per meter.
    // 1 INCH = 2,54 CM
    DisplayMetrics dm = new DisplayMetrics();
    mMainActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
    int dotsPerInch = dm.densityDpi;
    float dotsPerMeter = (float) (((dotsPerInch/2.54)*100)*mScaleFactor);
    mLogger.debug("--> New ScaleFactor defined in PositionService: " + dotsPerMeter + "pixels per meter.");
    mPositionService.setScale(dotsPerMeter);
  }

  public void setImageFilePath(String value)
  {
    mImageFilePath = value;
  }

  public String getImageFilePath()
  {
    return mImageFilePath;
  }

  public void setSetLocationMode(Boolean value)
  {
    mSetLocationMode = value;
  }

  public boolean isSetLocationMode()
  {
    return mSetLocationMode;
  }

  /**
   * Sets new position on map.
   * @param x Absolute x.
   * @param y Absolute y.
   */
  public void setPositionOnMap(float x, float y)
  {
    mLogger.debug("setPositionOnMap()");
    if (mPositionOnMap == null)
      mPositionOnMap = new Point();
    mPositionOnMap.set((int) Math.ceil(x), (int) Math.ceil(y));
  }

  /**
   * User has to set up the current position on map for init.
   * Method commit this position to PositionService.
   * @param x Absolute value.
   * @param y Absolute value.
   * @param view It is the main view
   */
  public void initPositionOnMap(float x, float y, Context view)
  {
    mLogger.debug("initPositionOnMap()");
    if (mPositionOnMap == null)
      mPositionOnMap = new Point();
    mPositionOnMap.set((int) Math.ceil(x), (int) Math.ceil(y));

    // Send the new position initialisation to the service
    mPositionService.setPosition(new Point(mPositionOnMap.x, mPositionOnMap.y));
  }

  public Point getPositionOnMap()
  {
    return mPositionOnMap;
  }

  public boolean isMapActive()
  {
    return mMapActive;
  }

  public void setMapActive(boolean value)
  {
    mMapActive = value;
  }

  public void PositionServicePause()
  {
    mPositionService.pause();
  }

  public void PositionServiceResume()
  {
    if (mBound && mPositionService.isPaused())
    {
      mLogger.debug("Resuming position service.");

      mPositionService.resume();
    }
  }

  /**
   * Get new Position from PositionService.
   */
  public void newPositionFromPositionService()
  {
    Point newPos = mPositionService.getCurrentImagePosition();
    if (newPos != null)
    {
      setPositionOnMap(newPos.x, newPos.y);
      mImageMap.invalidate();
    }
  }

  public void setImageMap(ImageView view)
  {
    mImageMap = view;
  }

  public void setMapBitmap(Bitmap bitmap)
  {
    mMapBitmap = bitmap;
  }

  public Bitmap getMapBitmap()
  {
    return mMapBitmap;
  }

}
