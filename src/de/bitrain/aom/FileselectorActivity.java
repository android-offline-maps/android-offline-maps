package de.bitrain.aom;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class FileselectorActivity extends ListActivity
{
  
  private final static Logger mLogger = LoggerFactory
    .getLogger(FileselectorActivity.class);

  private List<String> mItemList = null;
  private List<String> mPathFileList = null;
  private String mRootPath;
  private TextView mTextCurrentDirectory;

  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_fileselector);
    mTextCurrentDirectory = (TextView) findViewById(R.id.path);

    mRootPath = Environment.getExternalStorageDirectory().getPath();
    changeDirectory(mRootPath);
  }

  /**
   * Directory changed.
   * Refresh User-Interface and Directory-List.
   * @param path New directory-path
   */
  private void changeDirectory(String path)
  {
    // Update UI
    mTextCurrentDirectory.setText("Location: " + path);

    mItemList = new ArrayList<String>();
    mPathFileList = new ArrayList<String>();
    File f = new File(path);
    File[] files = f.listFiles();

    // Build directories
    if (!path.equals(mRootPath))
    {
      mItemList.add(mRootPath);
      mItemList.add("../");
      mPathFileList.add(mRootPath);
      mPathFileList.add(f.getParent());
    }

    // Build files
    for (int i = 0; i < files.length; i++)
    {
      File file = files[i];

      if (!file.isHidden() && file.canRead())
      {
        mPathFileList.add(file.getPath());
        if (file.isDirectory())
        {
          mItemList.add(file.getName() + "/"); // directory
        }
        else
        {
          mItemList.add(file.getName()); // file
        }
      }
    }

    ArrayAdapter<String> fileList =
      new ArrayAdapter<String>(this, R.layout.row, mItemList);
    setListAdapter(fileList);
  }

  /**
   * User select an item - if a list item is clicked
   */
  @Override
  protected void onListItemClick(ListView l, View v, int position, long id)
  {
    File file = new File(mPathFileList.get(position));

    if (file.isDirectory())
    {
      // Directory was selected
      if (file.canRead())
      {
        changeDirectory(mPathFileList.get(position));
      }
      else
      {
        new AlertDialog.Builder(this)
          .setIcon(R.drawable.ic_launcher)
          .setTitle("Can not read folder: " + file.getName())
          .setPositiveButton("OK", null)
          .show();
      }
    }
    else
    {
      // File was selected
      // Return data and close activity
      mLogger.debug("Map-File is selected. Build up return-object and close activity.");
      Intent returnIntent = new Intent();
      returnIntent.putExtra("AbsoluteFilePath", file.getAbsolutePath()); // AbsoluteFilePath: Identifier in parent activity
      setResult(RESULT_OK, returnIntent); // In parent activity onActivityResult() will be called
      this.finish();
    }
  }

}
