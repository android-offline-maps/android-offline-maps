package de.bitrain.aom.position;

import java.util.Observable;
import java.util.Observer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import de.bitrain.aom.helper.ObservableI;

/**
 * Service for keeping track of the user's GPS and image position.
 */
public class PositionService extends Service implements LocationListener,
  ObservableI
{
  /**
   * Internal binder class which offers the service to bound activities.
   */
  public class PositionServiceBinder extends Binder
  {
    public PositionService getService()
    {
      return PositionService.this;
    }
  }

  /**
   * Internal observable class.
   */
  private class PositionServiceObservable extends Observable
  {
    public void setChangedAndNotifyObservers()
    {
      setChanged();
      this.notifyObservers();
    }
  }

  private final static Logger mLogger = LoggerFactory
    .getLogger(PositionService.class);

  /**
   * Initial position in the image (pixel).
   */
  private Point mInitialImagePosition;

  /**
   * Current position in the image (pixel).
   */
  private Point mCurrentImagePosition;

  /**
   * GPS location.
   */
  private Location mLocationGps;

  private final PositionServiceObservable mObservable =
    new PositionServiceObservable();

  /**
   * Internal binder instance.
   */
  private final Binder mBinder = new PositionServiceBinder();

  /**
   * Whether the service is paused, i.e. whether it does not request position
   * updates.
   */
  private boolean mPaused = false;

  /**
   * Map scale.
   */
  private float mScale = 1.f;

  @Override
  public void addObserver(final Observer observer)
  {
    mObservable.addObserver(observer);
  }

  /**
   * Calculates the new image position for the given GPS location.
   */
  private Point calculateNewImagePosition(final Location location)
  {
    mLogger.debug("Calculating new image position.");

    // Calculate y difference
    final Location locationLatitude = new Location("PositionService");
    locationLatitude.setLongitude(mLocationGps.getLongitude());
    locationLatitude.setLatitude(location.getLatitude());

    final float distanceLatitude =
      mLocationGps.distanceTo(locationLatitude);
    final float distanceY = (float) (distanceLatitude * mScale
      * Math.signum(mLocationGps.getLatitude() - location.getLatitude()));

    mLogger.debug("Latitude distance between ({}, {}) and ({}, {}): {}; " +
      "y distance: {}", mLocationGps.getLatitude(),
      mLocationGps.getLongitude(),
      locationLatitude.getLatitude(), locationLatitude.getLongitude(),
      distanceLatitude, distanceY);

    // Calculate x difference
    final Location locationLongitude = new Location("PositionService");
    locationLongitude.setLongitude(location.getLongitude());
    locationLongitude.setLatitude(mLocationGps.getLatitude());

    final float distanceLongitude =
      mLocationGps.distanceTo(locationLongitude);
    final float distanceX = (float) (distanceLongitude * mScale
      * Math.signum(location.getLongitude() - mLocationGps.getLongitude()));

    mLogger.debug("Longitude distance between ({}, {}) and ({}, {}): {}; " +
      "x distance: {}", mLocationGps.getLatitude(),
      mLocationGps.getLongitude(),
      locationLongitude.getLatitude(), locationLongitude.getLongitude(),
      distanceLongitude, distanceX);

    // Get new image position
    final Point positionImageNew = new Point(mInitialImagePosition);
    positionImageNew.offset(Math.round(distanceX), Math.round(distanceY));

    mLogger.debug("New image position: ({}, {})", positionImageNew.x,
      positionImageNew.y);

    return positionImageNew;
  }

  @Override
  public int countObservers()
  {
    return mObservable.countObservers();
  }

  @Override
  public void deleteObserver(final Observer observer)
  {
    mObservable.deleteObserver(observer);
  }

  @Override
  public void deleteObservers()
  {
    mObservable.deleteObservers();
  }

  public Point getCurrentImagePosition()
  {
    return mCurrentImagePosition;
  }

  @Override
  public boolean hasChanged()
  {
    return mObservable.hasChanged();
  }

  public boolean isPaused()
  {
    return mPaused;
  }

  @Override
  public void notifyObservers()
  {
    mObservable.notifyObservers();

  }

  @Override
  public void notifyObservers(final Object data)
  {
    mObservable.notifyObservers(data);
  }

  @Override
  public IBinder onBind(final Intent arg0)
  {
    return mBinder;
  }

  /**
   * Handles location changes.
   */
  @Override
  public void onLocationChanged(final Location location)
  {
    mLogger.debug("Received location update. New location: {}, {}",
      location.getLongitude(), location.getLatitude());

    // If no location was previously set, save it as the starting location.
    if (mLocationGps == null)
    {
      mLogger.debug("Storing initial location.");
      mLocationGps = location;
    }

    // Calculate new image position
    mCurrentImagePosition = calculateNewImagePosition(location);

    mLogger.debug("Broadcasting position update.");

    // Notify observers of new position
    mObservable.setChangedAndNotifyObservers();
  }

  @Override
  public void onProviderDisabled(final String provider)
  {
    mLogger.debug("Provider disabled: {}", provider);
  }

  @Override
  public void onProviderEnabled(final String provider)
  {
    mLogger.debug("Provider enabled: {}", provider);
  }

  @Override
  public void onStatusChanged(final String provider, final int status,
    final Bundle extras)
  {
    mLogger.debug("Provider {} status change: {}", provider, status);
  }

  public void pause()
  {
    mLogger.debug("Pausing position service.");

    ((LocationManager) getBaseContext().getSystemService(
      Context.LOCATION_SERVICE)).removeUpdates(this);

    mPaused = true;

    return;
  }

  public void resume()
  {
    mLogger.debug("Requesting GPS location updates.");

    // Request GPS updates
    ((LocationManager) getBaseContext().getSystemService(
      Context.LOCATION_SERVICE)).requestLocationUpdates(
      LocationManager.GPS_PROVIDER, 10L, 0.f, this);

    mPaused = false;
  }

  public void setPosition(final Point startPosition)
  {
    mLogger.debug("Setting start position.");

    mInitialImagePosition = startPosition;
    // Reset GPS position
    mLocationGps = null;

    mLogger.debug("Starting position: {}, {}; scale: {}",
      mInitialImagePosition.x,
      mInitialImagePosition.y, mScale);
  }

  /**
   * Set the map scale.
   *
   * The scale is expected to be pixels per meter.
   */
  public void setScale(final float scale)
  {
    mScale = scale;
  }
}
