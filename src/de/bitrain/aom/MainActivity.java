package de.bitrain.aom;

import java.util.Observable;
import java.util.Observer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ZoomControls;

public class MainActivity extends Activity implements Observer
{
  private final static Logger mLogger = LoggerFactory
    .getLogger(MainActivity.class);

  /**
   * Starts the activity where images/maps can be loaded.
   * If the activity finishes, the onActivityResult()-Method
   * will be called.
   * @param view
   */
  public void loadImageMap_OnClick(final View view)
  {
    final Intent intent = new Intent(this, FileselectorActivity.class);
    this.startActivityForResult(intent, Constants.IMAGE_MAP_REQUEST);
  }

  /**
   * If an activity returns to this main activity, it will go to this method if 
   * the user has started the activity with startActivityForResult()
   *  
   */
  @Override
  protected void onActivityResult(final int requestCode, final int resultCode,
    final Intent data)
  {
    mLogger.debug("onActivityResult() at MainActivity.");
    switch (requestCode)
    {
      // Extract the absolute file-path and open the image/map
      case Constants.IMAGE_MAP_REQUEST:
        mLogger.debug("-> IMAGE_MAP_REQUEST");
        if ((resultCode == RESULT_OK) && (data != null))
        {
          mLogger.debug("-> IMAGE_MAP_REQUEST -> RESULT_OK");
          final String pathAbs = data.getStringExtra("AbsoluteFilePath");
          final Bitmap bitmap = BitmapFactory.decodeFile(pathAbs);
          Toast.makeText(this, "Datei geladen: " + pathAbs,
            Toast.LENGTH_LONG).show();
          Controller.getInstance().setImageFilePath(pathAbs);
          Controller.getInstance().setMapActive(true);
          Controller.getInstance().setMapBitmap(bitmap);
          
          Button btnConfig =
            (Button) findViewById(R.id.main_map_configuration_button);
          btnConfig.setVisibility(View.VISIBLE);
          Button btnLoc = (Button) findViewById(R.id.main_set_location_button);
          btnLoc.setVisibility(View.VISIBLE);
          ZoomControls zoom =
            (ZoomControls) findViewById(R.id.main_zoomControl);
          zoom.setVisibility(View.VISIBLE);
          TextView maintext = (TextView) findViewById(R.id.main_text);
          maintext.setText(R.string.main_text);

          mLogger.debug("-> IMAGE_MAP_REQUEST -> RESULT_OK -> DONE!");
          break;
        }
      case Constants.CONFIGURATION_REQUEST:
        mLogger.debug("-> CONFIGURATION_REQUEST");
        if ((resultCode == RESULT_OK) && (data != null))
        {
          Controller.getInstance().setScaleFactor(
            data.getFloatExtra("ConfigurationChanged", -1));
          final TextView textScale =
            (TextView) findViewById(R.id.main_text_scale);
          final float scalefactor = Controller.getInstance().getScaleFactor();
          if (scalefactor <= -1)
          {
            textScale.setText("Maßstab: N/A");
          }
          else
          {
            textScale.setText("Maßstab (m): 1:" + scalefactor);
          }
          Toast.makeText(this, "Neuer Maßstab: " + String.valueOf(scalefactor),
            Toast.LENGTH_LONG).show();
        }
      break;
    }
  }

  /**
   * Called when the activity is first created.
   */
  @Override
  public void onCreate(final Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    mLogger.debug("onCreate()");
    this.setContentView(R.layout.activity_main);

    Controller.getInstance().setImageMap(
      (ImageView) findViewById(R.id.main_mapImage));

    ZoomControls zoom = (ZoomControls) findViewById(R.id.main_zoomControl);
    zoom.setVisibility(View.INVISIBLE);
    zoom.setOnZoomInClickListener(new View.OnClickListener()
    {
      @Override
      public void onClick(final View v)
      {
        if (Controller.getInstance().isMapActive())
        {
          Controller.getInstance().incZoomFactor();
        }
      }
    });
    zoom.setOnZoomOutClickListener(new View.OnClickListener()
    {
      @Override
      public void onClick(final View v)
      {
        if (Controller.getInstance().isMapActive())
        {
          Controller.getInstance().decZoomFactor();
        }
      }
    });

    // Bind to position service 
    Controller.getInstance().bindService(this);
  }

  @Override
  public void onPause()
  {
    super.onPause();

    mLogger.debug("Pausing activity and position service.");

    Controller.getInstance().PositionServicePause();
  }

  @Override
  protected void onResume()
  {
    super.onResume();
    mLogger.debug("onResume()");

    // TODO Extend this condition if the service is only to be resumed under
    // certain circumstances
    Controller.getInstance().PositionServiceResume();
  }

  public void openConfiguration_OnClick(final View view)
  {
    if (Controller.getInstance().isMapActive())
    {
      final Intent intent = new Intent(this, MapConfigurationActivity.class);
      startActivityForResult(intent, Constants.CONFIGURATION_REQUEST);
    }
  }

  public void setLocationOnMap_OnClick(final View view)
  {
    if (Controller.getInstance().isMapActive())
    {
      Controller.getInstance().setSetLocationMode(
        !Controller.getInstance().isSetLocationMode());
      final Button buttonSetLocation =
        (Button) view.findViewById(R.id.main_set_location_button);
      if (Controller.getInstance().isSetLocationMode())
      {
        buttonSetLocation.setText(R.string.done);
      }
      else
      {
        buttonSetLocation.setText(R.string.main_set_location_button);
      }
    }
  }

  @Override
  public void update(final Observable observable, final Object data)
  {
    mLogger.debug("Observer was called. New map coordinates available.");
    Controller.getInstance().newPositionFromPositionService();
  }

}
