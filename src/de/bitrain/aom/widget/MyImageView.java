package de.bitrain.aom.widget;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.bitrain.aom.Controller;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;

public class MyImageView extends ImageView
{

  private final static Logger mLogger = LoggerFactory
    .getLogger(MyImageView.class);

  private float mLastTouchX = 0;
  private float mLastTouchY = 0;
  private float mX, mY;
  private final Paint mPaintPositionMarker;

  public MyImageView(Context context, AttributeSet attrs)
  {
    super(context, attrs);
    mPaintPositionMarker = new Paint();
    mPaintPositionMarker.setColor(Color.RED);
    mPaintPositionMarker.setStyle(Style.STROKE);
    mPaintPositionMarker.setStrokeWidth(3);
  }

  @Override
  public boolean onTouchEvent(MotionEvent event)
  {
    if (Controller.getInstance().getMapBitmap() == null)
      return true;

    if (Controller.getInstance().isSetLocationMode())
    {
      /**
       * User touched current location on map
       */
      // Transform to absolute map coordinates
      float x = event.getX();
      float y = event.getY();
      float zoom = Controller.getInstance().getZoomFactor();
      float xAbs = (x / zoom) - (mX / zoom);
      float yAbs = (y / zoom) - (mY / zoom);
      Controller.getInstance().initPositionOnMap(xAbs, yAbs, getContext());
      invalidate(); // redraw
    }
    else
    {
      /**
       * Normal interaction with map
       */
      switch (event.getAction())
      {
        case MotionEvent.ACTION_DOWN:
        {
          mLogger.debug("ACTION_DOWN at " + event.getX() + ", " + event.getY());
          mLastTouchX = event.getX();
          mLastTouchY = event.getY();
          break;
        }
        case MotionEvent.ACTION_MOVE:
        {
          float wegX = mLastTouchX - event.getX();
          float wegY = mLastTouchY - event.getY();
          mX = mX - wegX;
          mY = mY - wegY;

          invalidate();

          mLastTouchX = event.getX();
          mLastTouchY = event.getY();
          break;
        }
        default:
        {
          break;
        }
      }
    }
    return true;
  }

  @Override
  protected void onDraw(Canvas canvas)
  {
    super.onDraw(canvas);

    Bitmap b = Controller.getInstance().getMapBitmap();
    if (b != null)
    {
      canvas.translate(mX, mY);
      float scale = Controller.getInstance().getZoomFactor();
      canvas.scale(scale, scale);
      canvas.drawBitmap(b, 0, 0, null);

      // Draw position marker
      // Hint: Absolute value has NOT to be mapped to the current view
      // Tested with a gps coordinate. It has the same point on map
      // whether the scaling and position is different
      Point pt = Controller.getInstance().getPositionOnMap();
      if (pt != null)
      {
        canvas.drawCircle((pt.x),
          (pt.y), 5, mPaintPositionMarker);
      }
    }
  }

}
