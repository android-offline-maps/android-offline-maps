package de.bitrain.aom;

/**
 * Collection of application-wide constants.
 */
public final class Constants
{
  /**
   * Constant for intents containing the user position.
   */
  public static final String BROADCAST_POSITION =
    "de.bitrain.aom.BROADCAST_POSITION";

  /**
   * Constant for extra data contained in broadcasted position intents, contains
   * an integer array with the x and y position on the image.
   */
  public static final String EXTENDED_DATA_POSITION =
    "de.bitrain.aom.EXTENDED_DATA_POSITION";

  /**
   * Constants for request-codes if an activity will close
   * and the parent activity expect a result.
   */
  public static final int IMAGE_MAP_REQUEST = 0;
  public static final int CONFIGURATION_REQUEST = 1;
}
