Just some thoughts on the software (user stories):

1.) After the user starts the application, he is asked to
1.1) select an image representing the offline map to be used or
1.2) take a picture of the map to be used.

2.) After that, the user has to
2.1) specify the current location on the selected map and
2.2) specify the scale of the map

3.) Now the user can navigate the offline map.

4.) The user should be able to zoom in and out at any point and to a certain maximum / minimum level (depending on the map size and scale?).

5.) The user should be able to save maps for future re-use.

More (technical) details:

1.2) This would faciliate the camera of the mobile device, but would also be largely redundant, there are enough photo-taking-applications, there's no need to reinvent the wheel unless we plan some additional features.

2.2) This could be done by selecting two points on the map and specifying the (real) distance between them, from which the application could then infer the scale. Another possibility would be to let the user select two points and to let him walk from A to B and infer the scale from the distance walked.

5.) For saved maps, the scale should be stored, optionally past starting points and maybe even routes (routes could be stored separately).

Thoughts on the Software Architecture:

Activities:

MainActivity: Screen that is shown after the application is started. Contains choices for what to do next; for now, there is only one thing to do, and that's choosing an image file representing the map to be used. -> We need some file chooser, or alternatively some picture-taking application to allow the user to directly take a picture of the map. Later on, loading of previously used / stored maps should be possible.

MapConfigurationActivity: Here, the user has to enter the current location and scale of the map.

NavigationActivity: Here, the user can view / zoom the map and track their position. The user should be able to return to the configuration activity at any time. Uses the NavigationService to regularily update the user's position.

Services:

NavigationService: This service knows the map and the starting position of the user on the map, and offers the current position on the map based on the GPS coordinates.

----

Other:

- map scale: reality -> map -> image
- position gps -> image: first gps position -> current gps position, scale to length 1, multiply by scale and real distance between gps positions
